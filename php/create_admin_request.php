<?php

  // inserting headers here, instaed .htaccess file
  header("Access-Control-Allow-Origin: *");
  header("Content-Type: application/json; charset=UTF-8");
  header("Access-Control-Allow-Methods: POST");
  header("Access-Control-Max-Age: 15");
  header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

  // read raw POST data (JSON data) | sent as request payload from an api call"
  $data = json_decode(file_get_contents("php://input"));

  $request = $data->request;

  // create new request record in db
  // checking if payload contains specific request
  if($request == 'admin_request') {
  
    // autoload will load all classes that will be needed in this code
    include 'autoload.php';
    
    // get database connection
    $instance = Database::getInstance();
    $db = $instance->getConnection();
    
    $admin_request = new AdminRequestData($db);

    // set property values
    $admin_request->first_name = $data->first_name;
    $admin_request->last_name = $data->last_name;
    $admin_request->phone_number = $data->phone_number;
    $admin_request->email = $data->email;
    $admin_request->company_name = $data->company_name;
    $admin_request->company_address = $data->company_address;
    $admin_request->vat_number = $data->vat_number;
    $admin_request->registration_number = $data->registration_number;
    $admin_request->password = $data->password;
    
    // set mail sender values
    $sender = $data->first_name . ' ' . $data->last_name;
    $reg_key = $admin_request->getRegKey();
    
    var_dump($sender . " " . $reg_key);
    
    // create the request
    if(
      !empty($admin_request->first_name) &&
      !empty($admin_request->last_name) &&
      !empty($admin_request->phone_number) &&
      !empty($admin_request->email) &&
      !empty($admin_request->company_name) &&
      !empty($admin_request->company_address) &&
      !empty($admin_request->vat_number) &&
      !empty($admin_request->registration_number) &&
      !empty($admin_request->password) &&
       $admin_request->create()
    ) {
      
      // set response code
      http_response_code(200);

      $db = null;
      
      // Email  an activation link
      $to = "abpanov@gmail.com";							 
      $from = "accounts@panov.rs";
      $subject = 'New Admin Request Received!';
    //   $message = $sender . ' applied for admin account.<br>Click the link below to check and activate account:<br>
    //   <a href="http://localhost:8080">Click here to manage this user</a>';
      $message = $sender . ' applied for admin account.<br>Click the link below to check and activate account:<br>
      <a href="http://ifrs.panov.rs/activate.php?sender=' . $sender . '&reg_key=' . $reg_key . '">Click here to manage this user</a>';
      $headers = "From: $from\n";
      $headers .= "MIME-Version: 1.0\n";
      $headers .= "Content-type: text/html; charset=iso-8859-1\n";
      if(mail($to, $subject, $message, $headers)) {
          var_dump('mail poslat');
      }

      // display message: admin request created
      echo json_encode(array("message" => "Admin request created.", "connection" => $db));
    }

    // message if unable to create request
    else {

      // set response code
      http_response_code(400);

      $db = null;

      // display message: unable to create user
      echo json_encode(array("message" => "Unable to create request.", "connection" => $db));
    }
  } else {
      http_response_code(400);
      echo json_encode(array("message" => "Bad request"));
  }