<?php

  // check if given email exist in the database
  // inserting headers here, instaed of in .htaccess file
  header("Access-Control-Allow-Origin: *");
  header("Content-Type: application/json; charset=UTF-8");
  header("Access-Control-Allow-Methods: POST");
  header("Access-Control-Max-Age: 15");
  header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

  // read raw POST data (JSON data) | not available with enctype="multipart/form-data"
  $data = json_decode(file_get_contents("php://input"));

  $request = $data->request;

  if($request == 'email_exists') {

    // autoload classes
    include 'autoload.php';

    // get database connection
    $instance = Database::getInstance();
    $db = $instance->getConnection();

    $email_exists = new Email($db);

    // set property values
    $email_exists->email = $data->email;

    // check if email exists using Email class method
    if($email_exists->email_exists()) {

      $db = null;
      echo json_encode(array("message" => "email already registered", "connection" => $db));

      // return true because email exists in the database
      return true;
    } else {

      $db = null;
      echo json_encode(array("message" => "OK, this email can be registered.", "connection" => $db));

      // return false if email does not exist in the database
      return false;
    }
    return;
  }
//   Offed due to error returning by Vue | Postman on the other hand works as expected
//   http_response_code(400);
//   echo json_encode(array("message" => "Bad request"));
  