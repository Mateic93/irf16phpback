<?php

  // inserting headers here, instaed of in .htaccess file
  header("Access-Control-Allow-Origin: *");
  header("Content-Type: application/json; charset=UTF-8");
  header("Access-Control-Allow-Methods: POST");
  header("Access-Control-Max-Age: 15");
  header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

  // generate json web token
  include_once 'config/core.php';
  include_once 'libs/php-jwt-master/src/BeforeValidException.php';
  include_once 'libs/php-jwt-master/src/ExpiredException.php';
  include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
  include_once 'libs/php-jwt-master/src/JWT.php';
  use \Firebase\JWT\JWT;

  // read raw POST data (JSON data)
  $data = json_decode(file_get_contents("php://input"));

  $request = $data->request;

  if($request == 'key_check') {

    // autoload classes
    include 'autoload.php';

    // get database connection
    $instance = Database::getInstance();
    $db = $instance->getConnection();

    // instantiate user_login object
    $reg_key_check = new RegKeyCheck($db);

    // set product property values
    $reg_key_check->reg_key = $data->reg_key;
    //$reg_key_check = $reg_key_check->reg_key_check();
    // var_dump($reg_key_check->reg_key);
    // exit();
    
    if($reg_key_check->reg_key_check()) {
    
    //   $token = array(
    //     "iss" => $iss,
    //     "aud" => $aud,
    //     "iat" => $iat,
    //     // "nbf" => $nbf,
    //     "exp" => $exp,
    //     "data" => array(
    //       "key" => $reg_key_check->reg_key
    //     )
    //   );

      // set response code
      http_response_code(200);

    //   // generate jwt
    //   $jwt = JWT::encode($token, $key);
      
      echo json_encode(
        array(
            "message" => "Key exists",
            "bool" => true
            // "key" => $reg_key_check->reg_key
        )
      );

    } else {

      // set response code
      http_response_code(401);

      // tell the user login failed
      echo json_encode(
          array(
              "message" => "Key false",
              "bool" => false
          )
      );
    }
  }