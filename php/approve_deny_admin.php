<?php

//script for sending mass mails for payments

// inserting headers here, instaed of in .htaccess file
// include "config/headers.php";

include "autoload.php";

$data = json_decode(file_get_contents("php://input"));

$request = $data->request;
$new_status = $data->new_status;
$reg_key = $data->reg_key;

if($request == "approve_deny_admin"){
    //db instance
    $instance = Database::getInstance();
    $db = $instance->getConnection();
    $sql = "select * from admin_requests where reg_key = '" . $reg_key . "'";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    
    $output = [];
    $user = [];

        foreach($stmt as $row->$column){
            foreach($row as $c=>$v){
                foreach($v as $k=>$vp){
                    $user[$k] = $vp;
                }
            }
        }

    if($new_status == "accepted"){
        
        //u ovom delu treba da se promeni status u tabeli admin request
        $sql1 = "UPDATE admin_requests SET status = 'accepted' WHERE reg_key = '" . $reg_key . "'";
        $stmt1 = $db->prepare($sql1);
        $stmt1->execute();
        
        $link = "http://www.codehive.rs/php/admin_registration_complete.php?reg_key=" . $reg_key;
        
        
        
        $user_mail = $user["email"];
        $from = "codehiveAdmins@codehive.rs";
        $subject = "Your Admin Request has Been Accepted";
        $body = "<b>Dear " . $user["first_name"] . " ,</b> <br> <span>we are glad to inform you that your request has been approved
            and that hereby we're sending you a final step before you can make your account. <br> Please click on the link below to create your account:
            <br> </span>
            <a href='{$link}'>Click to confirm</a>
            <br>
            <hr>
            <p><b>SuperAdmin Name</b><br>
            Address line 1<br>
            011/2221111 </p>
            <img src='https://inteng-storage.s3.amazonaws.com/img/iea/9lwjAVnM6E/sizes/ocde_resize_md.jpg' width='300'>
        ";
        
        //zakomentarisano da ne salje mail
        $new_mail = new SendMail($from, $user_mail, $subject, $body);
        if($new_mail){
            $output = "Mail sent to the user that they have been accepted";
        }else {
            $output ="Something went wrong while sending the accepted mail";
        }
        
    }
    else if($new_status == "denied"){
        $sql1 = "UPDATE admin_requests SET status = 'denied' WHERE reg_key = '" . $reg_key . "'";
        $stmt1 = $db->prepare($sql1);
        $stmt1->execute();
        $user_mail = $user["email"];
        $decline_body = $data->decline_reason;
        $from = "codehiveAdmins@codehive.rs";
        $subject = "Rejection of your application for the program";
        
        //zakomentarisano da ne salje mail
        $new_mail = new SendMail($from, $user_mail, $subject, $decline_body);
        if($new_mail){
            $output = "Mail sent to the user that they have been declined";
        }else {
            $output ="Something went wrong while sending the rejection mail";
        }
    }
    else {
        die();
    }
    echo json_encode($output);
}