<?php

//script for sending mass mails for payments

// inserting headers here, instaed of in .htaccess file
include "config/headers.php";

include "autoload.php";

$data = json_decode(file_get_contents("php://input"));

$request = $data->request;

if($request == "mass_mails"){

    $subject = $data->subject;

    $body = $data->mail_body;
    
    // autoload classes
    include 'autoload.php';
    
    $instance = Database::getInstance();
    $db = $instance->getConnection();
    
    $stmt = $db->prepare("SELECT * from users");
    $stmt->execute();

    $users = [];
    $companies = [];
    $special_words = ["period"=>"March", "{user}" => "Matei", "{co_name}" => "test_company"];
    
    $months = ["", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "Novemeber", "December"];
    
    //looping trough all provided users from "users table"
    foreach($stmt as $k=>$user){
        $admin_id = $user["id"];
        $stmt1 = $db->prepare("SELECT * FROM `companies` WHERE main_admin = $admin_id");
        $stmt1 ->execute();
        $company = [];
        
        foreach($stmt1 as $ke=>$va){
            $company[] = $va;
        }
        $companies[] = $company;
        
        $month = date('m');
        $period = $month-1;
        
        
        $new_body = str_replace("{period}",$months[$period],$body);
        $new_body1 = str_replace("{user}", $user["first_name"], $new_body);
        $new_body2 = str_replace("{co_name}", strtoupper($company[0]["name"]), $new_body1);
        
        //This part needs to be automatic
        // foreach($special_word as $key => $word){
        //     $new_body = str_replace($key,$word,$body);
        // }
        $user["body"] = $new_body2;
        $users[] = $user;
    }
    $headers = 'From: php.mailing.test@gmail.com' . "\r\n" . 'MIME-Version: 1.0' . "\r\n" . 'Content-Type: text/html; charset=utf-8';

    $sucess_mails = 0;
    $failed_mails = 0;
    
    // foreach($users as $user){
    //     $email = $user["email"];
    //       $mail_body = $user["body"];
    //     $result = mail($email, $subject, $mail_body, $headers);
    //     if($result){
    //         $sucess_mails++;
    //     }else {
    //         $failed_mails++;
    //     }
    // }          

echo json_encode($sucess_mails);
}
else {
    echo "failed";
}


/*
Poštovani , 
U prilogu možete naci racune za zakup za 02/2020, 
po osnovu Ugovora o zakupu. 
Za sva dodatna pitanja možete se obratiti Aleksandri Milovanovic, 
na broj telefona +381 69 887 3289 e mail aleksandra.milovanovic@sporttime.rs 
S postovanjem, 
SPORT TIME BALKANS DOO 


"Poštovani " & Cells(cell.Row, "B").Value & "," _
      & vbNewLine & vbNewLine & _
        "U prilogu možete naci racune za zakup za " & Cells(cell.Row, "E").Value & "," _
        & vbNewLine & _
        "po osnovu Ugovora o zakupu." _
        & vbNewLine & _
        "Za sva dodatna pitanja možete se obratiti Aleksandri Milovanovic," _
        & vbNewLine & _
        "na broj telefona +381 69 887 3289 e mail aleksandra.milovanovic@sporttime.rs " _
        & vbNewLine & _
        "S postovanjem," _
        & vbNewLine & _
        "SPORT TIME BALKANS DOO"
