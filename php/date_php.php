<?php

$input_date = 20122020;
$label = "DD/MM/YYYY";

// $data = json_decode(file_get_contents("php://input"));

// $label = $data->label;
// $input_date = $data->input_date;

function get_input_date($input_date, $label){

  $date_arr = str_split(strval($input_date));

  $year = $date_arr[4] . $date_arr[5] . $date_arr[6] . $date_arr[7];

  if($label == "DD/MM/YYYY"){
    $day = $date_arr[0] . $date_arr[1];
    $month = $date_arr[2] . $date_arr[3];
  }
  else if($label == "MM/DD/YYYY"){
    $day = $date_arr[2] . $date_arr[3];
    $month = $date_arr[0] . $date_arr[1];
  }

  if(intval($month) > 12){
    die("Month can not be higher than 12");
  }

  if(intval($day) < 1){
    die("Day can not be a 0");
  }else if(intval($day) > 31){
    die("So far we only have 31 days in any given month");
  }

  if(intval($year) > 2200){
    die("That's too far in the future mate, we'll make a new program by that time");
  }else if(intval($year) < 1000){
    die("Hmm, you have some really old document there, care to show it to us for evaluation? ");
  }

  $date_format = $year . "-" . $month . "-" . $day;
  
  return $date_format;
}
echo get_input_date($input_date, $label);