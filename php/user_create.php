<?php

  // inserting headers here, instaed .htaccess file
  include "scripts/headers.php";

  // read raw POST data (JSON data) | sent as request payload from an api call"
  $data = json_decode(file_get_contents("php://input"));

  $request = $data->request;

  // create new request record in db
  // checking if payload contains specific request
  if($request == 'user_create') {
  
    // autoload will load all classes that will be needed in this code
    include 'autoload.php';
    
    // get database connection
    $instance = Database::getInstance();
    $db = $instance->getConnection();
    $list = ["first_name", "last_name", "username", "email", "password", "user_status", "user_level"];
    
    //create the new object
    $user = new User($db);
    // set property values
    foreach($list as $index=>$name){
      $user->$name = $data->name;
    }

    /*
    We need to do 2 following ifs here:
    1) If the super admin of the new company is trying to create a new super adming which we don't allow

    2) If the company already exceeds the max amount of users for this given profile

    */

    // create the user
    if(
      !empty($user->first_name) &&
      !empty($user->last_name) &&
      !empty($user->username) &&
      !empty($user->email) &&
      !empty($user->password) &&
      !empty($user->user_status) &&
      !empty($user->user_level) &&
      $user->create()
    ) {

      // set response code
      http_response_code(200);

      $db = null;

      // display message: user was created
      echo json_encode(array("message" => "User created.", "connection" => $db));
    }

    // message if unable to create user
    else {

      // set response code
      http_response_code(400);

      $db = null;

      // display message: unable to create user
      echo json_encode(array("message" => "Unable to create user.", "connection" => $db));
    }

  }