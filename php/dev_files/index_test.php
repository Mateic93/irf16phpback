
<?php

// read raw POST data (JSON data) | not available with enctype="multipart/form-data"
// $data = json_decode(file_get_contents("php://input"));

$data = new stdClass;
$data->date = 13122015;
$data->label = "Day/Month/Year";
$data->request = "date_php";

$request = $data->request;

// create new request record in db
if($request == 'date_php') {

  // autoload classes
  include 'autoload.php';

  // connect to database
  $instance = Database::getInstance();
  $conn = $instance->getConnection();

  $date = $data->date;
  $label = $data->label;


  $table_name = "date";

  // insert query
  $query = "INSERT INTO " . $table_name . "
  SET
    date = :date";

  // prepare the query
  $stmt = $conn->prepare($query);


  // bind the values
  $stmt->bindParam(':date', $date);

  // execute the query, also check if query was successful
  if($stmt->execute()) {
    echo "yes ";
    $stmt = null;
    $instance = null;
    $conn = null;
  } else {
    echo "noooo ";
    $stmt = null;
    $instance = null;
    $conn = null;
  }

  var_dump($conn);
}