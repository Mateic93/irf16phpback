<?php

  class RandKey2 {

    private $conn;
    private $table_name = "admin_requests";
    private $reg_key;

    public function __construct($db){
        $this->conn = $db;
    }

    public function rand_key_maker() {
        
        // $this->reg_key = bin2hex(random_bytes(3));
        $this->reg_key = mt_rand(1, 3);

      // query to check if email exists
      $query = "SELECT id FROM " . $this->table_name . " WHERE reg_key = ? LIMIT 0,1";

      // prepare the query
      $stmt = $this->conn->prepare($query);

      // bind given key
      $stmt->bindParam(1, $this->reg_key);

      // execute the query
      $stmt->execute();

      // get number of rows
      $row = $stmt->rowCount();
      var_dump('Klasa1: prondjen key duplikat u bazi: '.$row);
      
      // if we have same random key
      if($row > 0) {
          var_dump('Klasa1: duplikat key u bazi ima vrednost: '.$this->reg_key); 
        $this->rand_key_maker();
      } else {
           var_dump('Klasa1: trenutno generisan reg_key: '.$this->reg_key);
        return $this->reg_key;
      }
    }
  }