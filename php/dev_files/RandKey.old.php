<?php

  // this class is static so we can avoid instantiation | it is faster to type ::
  class RandKey {
    protected static $rand_key;

    public static function rand_key_maker() {

      // self::$rand_key = mt_rand(100000, 999999); | option to randomize only numbers

      self::$rand_key = bin2hex(random_bytes(3));

      // here goes db query to check if by any chance randomly created key exist in db
      // and if there is we reqursively call rand_key_maker() again
      // else
      return self::$rand_key;
    }
  }