<?php

  // required headers
  // header("Access-Control-Allow-Origin: http://localhost/rest-api-authentication-example/");
  header("Access-Control-Allow-Origin: *");
  header("Content-Type: application/json; charset=UTF-8");
  header("Access-Control-Allow-Methods: POST");
  header("Access-Control-Max-Age: 3600");
  header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

  // read raw POST data (JSON data) | not available with enctype="multipart/form-data"
  $data = json_decode(file_get_contents("php://input"));

  $request = $data->request;

  // create new request record in db
  if($request == 'requestReg') {

    // autoload classes
    include 'autoload.php';

    // connect to database
    $instance = Database::getInstance();
    $conn = $instance->getConnection();

    $first_name = $data->first_name;
    $last_name = $data->last_name;
    $phone_number = $data->phone_number;
    $email = $data->email;
    $company_name = $data->company_name;
    $company_address = $data->company_address;
    $vat_number = $data->vat_number;
    $registration_number = $data->registration_number;
    $signup = date('Y-m-d H:i:s');

    $table_name = "register_requests";

    // insert query
    $query = "INSERT INTO " . $table_name . "
    SET
      first_name = :first_name,
      last_name = :last_name,
      phone_number = :phone_number,
      email = :email,
      company_name = :company_name,
      company_address = :company_address,
      vat_number = :vat_number,
      registration_number = :registration_number,
      signup = :signup";

    // prepare the query
    $stmt = $conn->prepare($query);

    // basic sanitize | adding more later...
    $first_name = htmlspecialchars(strip_tags($first_name));
    $last_name = htmlspecialchars(strip_tags($last_name));
    $phone_number = htmlspecialchars(strip_tags($phone_number));
    $email = htmlspecialchars(strip_tags($email));
    $company_name = htmlspecialchars(strip_tags($company_name));
    $company_address = htmlspecialchars(strip_tags($company_address));
    $vat_number = htmlspecialchars(strip_tags($vat_number));
    $registration_number = htmlspecialchars(strip_tags($registration_number));
    // $signup = htmlspecialchars(strip_tags($signup));

    // bind the values
    $stmt->bindParam(':first_name', $first_name);
    $stmt->bindParam(':last_name', $last_name);
    $stmt->bindParam(':phone_number', $phone_number);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':company_name', $company_name);
    $stmt->bindParam(':company_address', $company_address);
    $stmt->bindParam(':vat_number', $vat_number);
    $stmt->bindParam(':registration_number', $registration_number);
    $stmt->bindParam(':signup', $signup);

    // execute the query, also check if query was successful
    if($stmt->execute()) {
      echo "yes ";
      $stmt = null;
      $instance = null;
      $conn = null;
    } else {
      echo "noooo ";
      $stmt = null;
      $instance = null;
      $conn = null;
    }

    var_dump($conn);
  }