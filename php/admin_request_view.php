<?php

//script for sending mass mails for payments

// inserting headers here, instaed of in .htaccess file
// include "config/headers.php";

include "autoload.php";

$data = json_decode(file_get_contents("php://input"));

$request = $data->request;

if($request == "all_admin_requests"){
    //db instance
    $instance = Database::getInstance();
    $db = $instance->getConnection();
    
    //column name for ease of usage -> could be improved to be automatic
    $columns = ["id", "reg_key", "first_name", "last_name", "phone_number", "email", "company_name", "company_address", "company_city", "company_country", "vat_number", "registration_number", "created", "status"];
    
    $sql_stm = "select ";
    foreach($columns as $column){
        if($column == "status"){
            $sql_stm .= $column;
        }else {
          $sql_stm .= $column . ","; 
        }
        
    }
    $sql_stm .= " from admin_requests";
    // $sql = "select id, reg_key, first_name, last_name, phone_number, email, company_name, company_address, city, country, vat_number, registration_number, created, status from admin_requests";
    
    //selecting required data without password
    $stmt = $db->prepare($sql_stm);
    $stmt->execute();
    
    //both used for payload
    $requests_pending = []; 
    $requests_accepted = []; 
    
    //looping trough each statement
    foreach($stmt as $rows=>$col){
        
        //creating a new "object" for each
        $new_admin = [];
        
        //looping trough the previously named $columns array and adding the values to $new_admin
        foreach($columns as $column){
            $col_name = $column;
            $column = $col[$column];
            $new_admin[$col_name] = $column;
        }
        
        
        //checking if the status is pending or accepted and adding it to the correct predefined array
        foreach($col as $c=>$v){
            if($c == "status" && $v == "pending"){
                $requests_pending[] = $new_admin;
                $req_test[] = "test";
            }else if($c == "status" && $v == "accepted"){
                $requests_accepted[] = $new_admin;
            }
        }
    }
    //output
    $requests = ["pending" => $requests_pending, "accepted" => $requests_accepted]; // na kraju ih zajedno ovde pakujem
    echo json_encode($requests);
}else {
    echo "something went wrong";
}
