<?php

  class AdminRequestData {
  
    // database connection and table name
    private $conn;
    protected $reg_key;
    private $created;
    private $table_name = "admin_requests";

    // object properties | they are public because they are initially empty and their values will be asigned later from payload
    public $first_name;
    public $last_name;
    public $phone_number;
    public $email;
    public $company_name;
    public $company_address;
    public $vat_number;
    public $registration_number;
    public $password;

    // constructor
    public function __construct($db){
      $this->conn = $db;

      // new approach
      // $instance = Database::getInstance();
      // $db = $instance->getConnection();
      $db_reg_key = new RegKey($db, $this->table_name);
      $this->reg_key = $db_reg_key->reg_key_maker();
      
      // svojevrsni promise
      // obzirom da RandKey klasa radi rekurziju ukoliko nadje isti broj (key) u bazi, ponavljace je sve dok ne napravi jedinstven broj (key)
      // medjutim odmah nakon te poslednje rekurzije prvo sto ce vratiti je NULL (ne null, ne '' ili bilo sta slicno)
      // tako da ga ja ovim while uslovom "nateram" da ponovi metodu instance RegKey
      // tekoreci, jurice se sve dok iz prve novopozvana metoda ne vrati jedinstveni broj (key)
      while($this->reg_key == NULL) {
          var_dump('Klasa2: dobijen je NULL pa kroz while resetujem i pozivam inicijalnu metodu ispocetka');
          $this->reg_key = $db_reg_key->reg_key_maker();
      }
    //   if($this->reg_key == NULL) {
    //       $this->reg_key = $db_reg_key->rand_key_maker();
    //   }
      var_dump('Klasa2: dobijen je konacan output: '.$this->reg_key); // exit;

      // $this->reg_key = RandKey::rand_key_maker();
      $this->created = date('Y-m-d H:i:s');
    }

    public function getRegKey() {
      return $this->reg_key;
    }

    // create new user record
    public function create() {
      // exit;
      // insert query
      $query = "INSERT INTO " . $this->table_name . "
      SET
        reg_key = :reg_key,
        first_name = :first_name,
        last_name = :last_name,
        phone_number = :phone_number,
        email = :email,
        company_name = :company_name,
        company_address = :company_address,
        vat_number = :vat_number,
        registration_number = :registration_number,
        created = :created,
        password = :password";

      // prepare the query
      $stmt = $this->conn->prepare($query);

      // basic sanitize | adding more later...
      if (filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
        $this->email = $this->email; // without checking if email exists, only this line goes here
      } else {
        echo json_encode(array("message" => "Email not valid."));
        exit;
      }
      $this->first_name = htmlspecialchars(strip_tags($this->first_name));
      $this->last_name = htmlspecialchars(strip_tags($this->last_name));
      $this->phone_number = htmlspecialchars(strip_tags($this->phone_number));
      $this->company_name = htmlspecialchars(strip_tags($this->company_name));
      $this->company_address = htmlspecialchars(strip_tags($this->company_address));
      $this->vat_number = htmlspecialchars(strip_tags($this->vat_number));
      $this->registration_number = htmlspecialchars(strip_tags($this->registration_number));
      $this->password=htmlspecialchars(strip_tags($this->password));
      // $signup = htmlspecialchars(strip_tags($signup));

      // hashing password
      $password_hash = password_hash($this->password, PASSWORD_BCRYPT);
      
      // bind the values
      $stmt->bindParam(':reg_key', $this->reg_key);
      $stmt->bindParam(':first_name', $this->first_name);
      $stmt->bindParam(':last_name', $this->last_name);
      $stmt->bindParam(':phone_number', $this->phone_number);
      $stmt->bindParam(':email', $this->email);
      $stmt->bindParam(':company_name', $this->company_name);
      $stmt->bindParam(':company_address', $this->company_address);
      $stmt->bindParam(':vat_number', $this->vat_number);
      $stmt->bindParam(':registration_number', $this->registration_number);
      $stmt->bindParam(':created', $this->created);
      $stmt->bindParam(':password', $password_hash);

      // execute the query, also check if query was successful
      if($stmt->execute()){
        return true;
      }
      return false;
    }

  }