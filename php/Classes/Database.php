<?php

// Singleton class
class Database {
  
  private static $instance = null;
  private $conn;
  
  private $host = 'localhost';
  private $user = 'panovrs_ifrs';
  private $pass = 'FugHXZ9-BL^1';
  private $name = 'panovrs_ifrs';
   
  // The db connection is established in the private constructor.
  private function __construct() {
    $this->conn = new PDO("mysql:host={$this->host}; dbname={$this->name}", $this->user, $this->pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
  }
  
  public static function getInstance() {
    if(!self::$instance) {
      self::$instance = new Database();
    }
    return self::$instance;
  }
  
  public function getConnection() {
    return $this->conn;
  }

  // Disable the cloning of this class
  final public function __clone() {
    throw new Exception('Feature disabled.');
  }

  // Disable the wakeup of this class if someone tries to serialize object
  final public function __wakeup() {
    throw new Exception('Feature disabled.');
  }
}