<?php

  class Email {
    private $conn;
    private $table_name = "admin_requests";

    // object properties
    public $email;

    // constructor
    public function __construct($db){
      $this->conn = $db;
    }

    public function email_exists() {

      // query to check if email exists
      $query = "SELECT id FROM " . $this->table_name . " WHERE email = ? LIMIT 0,1";

      // prepare the query
      $stmt = $this->conn->prepare($query);

      // sanitize
      if (filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
        $this->email = $this->email;
      } else {
        echo json_encode(array("message" => "Email not valid."));
        exit;
      }

      // bind given email value
      $stmt->bindParam(1, $this->email);

      // execute the query
      $stmt->execute();

      // get number of rows
      $row = $stmt->rowCount();

      // if we have a row an email is already registered
      if($row > 0) {
        return true;
      }
      return false;
    }
  }