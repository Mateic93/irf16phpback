<?php

  class UserLogin {

    // database connection and table name
    private $conn;
    private $table_name = "users";

    // object properties
    public $id;
    public $company_id;
    public $role_id;
    public $first_name;
    public $last_name;
    public $email;
    public $password;
    public $reg_key;

    // constructor
    public function __construct($db) {
      $this->conn = $db;
    }

    // check if given email exist in the database
    public function emailExists() {
    
      // query to check if email exists
      $query = "SELECT id, company_id, role_id, first_name, last_name, password, session_key
        FROM " . $this->table_name . "
        WHERE email = ?
        LIMIT 0,1";

      // prepare the query
      $stmt = $this->conn->prepare($query);

      // sanitize
      if (filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
        $this->email = $this->email;
      } else {
        echo json_encode(array("message" => "Email not valid."));
        exit;
      }

      // bind given email value
      $stmt->bindParam(1, $this->email);

      // execute the query
      $stmt->execute();

      // get number of rows
      $num = $stmt->rowCount();

      // if email exists, assign values to object properties for easy access and use for php sessions
      if($num > 0) {

        // get record details / values
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // assign values to object properties
        $this->id = $row['id'];
        $this->company_id = $row['company_id'];
        $this->role_id = $row['role_id'];
        $this->first_name = $row['first_name'];
        $this->last_name = $row['last_name'];
        $this->password = $row['password'];
        $this->reg_key = $row['session_key'];

        // return true because email exists in the database
        return true;
      }

      // return false if email does not exist in the database
      return false;
    }
  }