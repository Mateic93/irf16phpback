<?php

  class User {

    // database connection and table name
    private $conn;
    private $table_name = "users";

    // object properties | they are public because they are initially empty and their values will be asigned later from payload
    private $first_name;
    private $last_name;
    private $username;
    private $email;
    private $password;
    private $user_status = 1;
    private $user_level;

    private $list = ["first_name", "last_name", "username", "email", "password", "user_status", "user_level"];
    // constructor
    public function __construct($db) {
      $this->conn = $db;
    }

    public function create(){
      $query = "INSERT INTO " . $this->table_name . "
      SET ";
      
      foreach($this->list as $index=>$name){
        $query .= $name . " = :" . $name;
      }
      $query .= '"';

      $stmt = $this->conn->prepare($query);


      // basic sanitize | adding more later...
      if (filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
        $this->email = $this->email;
      } else {
        echo json_encode(array("message" => "Email not valid."));
        // $this->email = "temporary@mail.com";
        exit;
      }
      foreach($this->list as $index=>$name){
        $this->$name = htmlspecialchars(strip_tags($this->$name));
        $param_binder = ":".$name;
        $stmt->bindParam($param_binder , $this->$name);
      }
      // $signup = htmlspecialchars(strip_tags($signup));

      // hashing password
      $password_hash = password_hash($this->password, PASSWORD_BCRYPT);

      // bind the values
      $stmt->bindParam(':password', $password_hash);

      // execute the query, also check if query was successful
      if($stmt->execute()){
        return true;
      }
      return false;
    }
    public function change(){

    }
}