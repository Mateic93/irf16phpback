<?php

class NewEmployee {
    
    private $db;
    private $email = "";
    private $admin = "";
    private $privilege = [];
    private $admin_id;
    private $admin_email;
    
    //Parameteres that are expected
    public function __construct($db, $admin, $email, $privilege){
        $this->db = $db;
        $this->email = $email;
        $this->admin = $admin;
        $this->privilege = $privilege;
        
        //order of functions when the class is instanced "get_admin_data, send_invitation"
        $this->get_admin_data();
        $this->send_invitation($email);
        
        // $test_data = ["email" => $this->email];
        // echo json_encode($test_data);
        
        
    }
    
    public function get_admin_data(){
        $db = $this->db;
        $admin = $this->admin;
        
        $sql = "select * from users where session_key = '{$admin}' and role_id = '2'";
        
        $stmt = $db->prepare($sql);
        $stmt->execute();
        
        $admin_data = [];
        
        
        //adding the data to $admin_data for later usage
        foreach($stmt as $c=>$v){
            $admin_data[] = $v;
        }
        
        //assigning the values for use in later functions
        $this->company_id = $admin_data[0]["company_id"];
        $this->admin_id = $admin_data[0]["id"];
        $this->admin_email = $admin_data[0]["email"];
        
        // echo json_encode($admin);
        // echo json_encode($admin_data);
        // echo $this->admin_id;
        // echo json_encode($this->company_id);
    }
    
    public function send_invitation($email){
        
        // echo $this->company_id;
        
        $db = $this->db;
        $admin_id = $this->admin_id;
        $email_new = $email;
        
        $company_id = $this->company_id;
        $created = date("Y-m-d h:m:s");
        $status = "pending";
        $last_sent = date("Y-m-d h:m:s");
        $no_sent = "1";
        
        
        $test_data = [
            "email" => $email_new,
            "admin_id" => $admin_id,
            "company_id" => $company_id,
            "created" => $created,
            "status" => $status,
            "l_sent" => $last_sent,
            "no_sent" => $no_sent
            ];
            
        
        $sql = "INSERT INTO `employee_requests` (`id`, `email`, `admin_id`, `company_id`, `created`, `status`, `last_sent`, `no_sent`) VALUES (NULL, '{$email}', '{$admin_id}', '{$company_id}', '{$created}', '{$status}', '{$last_sent}', '{$no_sent}');";
        
        // $sql1 = "INSERT INTO `employee_requests` (`id`, `email`, `admin_id`, `company_id`, `created`, `status`, `last_sent`, `no_sent`) VALUES (NULL, 'matei@test.com', '10', '14', '2020-04-19 21:31:20', 'pending', '', '1');";
        
        // $sql2 = "INSERT INTO `employee_requests` (`id`, `email`, `admin_id`, `company_id`, `created`, `status`, `last_sent`, `no_sent`) VALUES (NULL, 'test', '10', '14', '2020-04-19 21:31:20', 'pending', '', '1');";
        
        $stmt = $db->prepare($sql);
        
        // echo json_encode($test_data);
        $stmt->execute();
        
        $admin_mail = $this->admin_email;
        $subject = "Link to register for the ifrs app";
        //($from, $to, $subject, $body)
        $body = "<p>Dear,  <br><br> You have been invited to register to the ifrs application using the following link: <br><a href='https://giphy.com/gifs/thumbs-up-111ebonMs90YLu'>Click Here!</a>.<br> <br>For any further information please contact your system Administrators.<hr><b><u>Ifrs Signature (or admin sign)</u></b><p>";
        
        $headers = 'MIME-Version: 1.0' . "\r\n" . 'Content-Type: text/html; charset=utf-8';
        mail($email_new, $subject, $body, $headers);
    }
    
}