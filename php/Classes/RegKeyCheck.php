<?php

  class RegKeyCheck {

    // database connection and table name
    private $conn;
    private $table_name = "admin_requests";

    // object properties | they are public because they are initially empty and their values will be asigned later from payload
    public $reg_key;

    // constructor
    public function __construct($db) {
      $this->conn = $db;
    }

    // check if given key exist in the database
    public function reg_key_check() {
    //var_dump($this->reg_key);
      // query to check if key exists
      $query = "SELECT reg_key FROM $this->table_name WHERE reg_key = '$this->reg_key'";

      // prepare the query
      $stmt = $this->conn->prepare($query);

      // sanitize
      
      $this->reg_key = htmlspecialchars(strip_tags($this->reg_key));

      // bind given value
      $stmt->bindParam(1, $this->reg_key);

      // execute the query
      $stmt->execute();

      // get number of rows
      $num = $stmt->rowCount();

      // if email exists, assign values to object properties for easy access and use for php sessions
      if($num > 0) {

        // get record details / values
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // assign values to object properties
        $this->reg_key = $row['reg_key'];

        // return true because key exists in the database
        return true;
      }

      // return false if key does not exist in the database
      return false;
    }
  }