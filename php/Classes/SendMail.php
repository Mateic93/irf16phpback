<?php

class SendMail{
    public $from;
    public $to;
    public $subject;
    public $body;
    
    
    public function __construct($from, $to, $subject, $body) {
        // $this->from = $from;
        // $this->to = $to;
        // $this->subject = $subject;
        // $this->body = $body;
        
        $this->send_mail($from, $to, $subject, $body);
    }
    
    public function send_mail($from, $to, $subject, $body){
        $headers = 'From: '. $from . "'". "\r\n" . 'MIME-Version: 1.0' . "\r\n" . 'Content-Type: text/html; charset=utf-8';
        mail($to, $subject, $body, $headers);
    }
    
}