<?php

class RegisterEmployee {

    private $admin_id;
    private $first_name;
    private $last_name;
    private $password;
    private $email;
    private $mobile;
    private $db;
    private $emp_req_data;
    
    public function __construct($db, $data){
        $this->db = $db;
        $this->first_name = $data->first_name;
        $this->last_name = $data->last_name;
        $this->password = $data->last_name;
        $this->email = $data->email;
        $this->mobile = $data->mobile;
        
        $test_data = ["email" => $this->email, "first_name" => $this->first_name, "last_name" => $this->last_name, "password" => $this->password, "email" => $this->email, "mobile" => $this->mobile];
        // echo json_encode($test_data);
        
        if($this->if_exists($this->email)){
            $this->register_employee();
        }else {
            echo "ne sljaka";
        }
        
    }
    
    public function if_exists($email){
        $db = $this->db;
        $email = $this->email;
        $sql = "SELECT * FROM `employee_requests` WHERE email = '{$email}'";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        
        $user_exists = [];
        foreach($stmt as $k=>$v){
            $user_exists[] = $v;
        }
        
        $this->emp_req_data = $user_exists;
        if(!empty($user_exists)){
            return true;
        }
    }
    
    public function register_employee(){
        // echo json_encode($this->emp_req_data);
        
        $db = $this->db;
        $company_id = $this->emp_req_data[0]["company_id"];
        $created = date("Y-m-d h:m:s");
        // echo $company_id;
        // echo json_encode($this->emp_req_data);
        $sql = "INSERT INTO `users` (`id`, `session_key`, `role_id`, `company_id`, `first_name`, `last_name`, `email`, `password`, `phone_number`, `created`, `last_login`, `removed`) VALUES (NULL, 'wa2155', '3', '{$company_id}', '{$this->first_name}', '{$this->last_name}', '{$this->email}', '{$this->password}', '{$this->mobile}', '{$created}', NULL, '0');";
        $stmt = $db->prepare($sql);
        $new_user = $stmt->execute();
        if($new_user){
            $sql1 = "UPDATE `employee_requests` SET `status` = 'accepted' WHERE `employee_requests`.`email` = '{$this->email}';";
            $stmt1 = $db->prepare($sql1);
            $val1 = $stmt1->execute();
            if($val1){
                echo "status updated";
            }else {
                echo "status wasn't updated";
            }
        }else {
            $message = ["error_message" => "Failed to insert"];
            echo json_encode($message);
        }
    }
    
}