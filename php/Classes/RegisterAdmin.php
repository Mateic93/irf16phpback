<?php

class RegisterAdmin {
    
    private $reg_key;
    private $db;
    private $admin_data = [];
    private $company_id;
    
    public function __construct($db, $reg_key){
        $this->reg_key = $reg_key;
        $this->db = $db;
        $this->get_registration($this->reg_key);
        $this->create_a_company($this->admin_data);
        $this->register_admin_as_user($this->admin_data);
        
    }
    
    
    public function get_registration($reg_key){
        
        $db = $this->db;
        $query = "select * from admin_requests where reg_key = '{$this->reg_key}'";
        $stmt = $db->prepare($query);
        
        $stmt->execute();
        
        
        $query2 = "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'admin_requests'";
        $stmt2 = $db->prepare($query2);
        $stmt2->execute();
        
        $column_names = [];
        
        foreach($stmt2 as $row=>$col){
            $column_names[] = $col["column_name"];
        }
        
        
        $new_admin = [];
        foreach($stmt as $row=>$column){
            $new_admin[] = $column;
        }
        
        $new_admin_clear = [];
        
        foreach($column_names as $no=>$col){
            $new_admin_clear[$col] = $new_admin[0][$col];
        }
        
        $this->admin_data = $new_admin_clear;
        $this->ar_columns = $column_names;
    }
    
    public function register_admin_as_user($admin){
        $db = $this->db;
        $session_key = $this->reg_key; //ovaj deo ne valja posto mora da se generise novi
        $company_id = $this->company_id;
        $first_name = $admin["first_name"];
        $last_name = $admin["last_name"];
        $email = $admin["email"];
        $password = $admin["password"];
        $phone_number = $admin["phone_number"];
        $created = date("Y-m-d h:m:s");
        $last_login = NULL;
        $removed = 0;
        
        $sql = "INSERT INTO `users` (`id`, `session_key`, `role_id`, `company_id`, `first_name`, `last_name`, `email`, `password`, `phone_number`, `created`, `last_login`, `removed`) VALUES (NULL, '{$session_key}', '2', '{$company_id}', '{$first_name}', '{$last_name}', '{$email}', '{$password}', '{$phone_number}', '{$created}', NULL, '0')";
        
        $stmt1 = $db->prepare($sql);
        
        $stmt1->execute();
    }
    
    public function create_a_company($admin){
        $db = $this->db;
        $name = $this->admin_data["company_name"];
        $address = $this->admin_data["company_address"];
        $city = $this->admin_data["company_city"];
        $country = $this->admin_data["company_country"];
        
        $test_data = [$name, $address, $city, $country];
        
        
        $sql = "INSERT INTO `companies` (`id`, `name`, `adress`, `city`, `country`) VALUES (NULL, '{$name}', '{$address}', '{$city}', '{$country}');";
        
        $stmt = $db->prepare($sql);
        

        $stmt->execute();
        $last_id = $db->lastInsertId();
        $this->company_id = $last_id;
    }
}

