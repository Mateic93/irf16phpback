<?php

include "autoload.php";

$data = json_decode(file_get_contents("php://input"));

$request = $data->request;
$admin = $data->admin_reg;
$payload = $data->list_of_emails;

if($request == "employee_invite"){
    $instance = Database::getInstance();
    $db = $instance->getConnection();
    
    $test_emp = [];
    
    foreach($payload as $new_employee){
        $new_admin = $admin;
        $email = $new_employee->email;
        $privilege = $new_employee->privilege;

        $new_emp = new NewEmployee($db, $new_admin, $email, $privilege);
        // $test_emp[] = $new_emp->email;
    }
    // echo json_encode($test_emp);
}