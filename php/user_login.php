<?php

  // inserting headers here, instaed of in .htaccess file
  header("Access-Control-Allow-Origin: *");
  header("Content-Type: application/json; charset=UTF-8");
  header("Access-Control-Allow-Methods: POST");
  header("Access-Control-Max-Age: 15");
  header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

  // generate json web token
  include_once 'config/core.php';
  include_once 'libs/php-jwt-master/src/BeforeValidException.php';
  include_once 'libs/php-jwt-master/src/ExpiredException.php';
  include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
  include_once 'libs/php-jwt-master/src/JWT.php';
  use \Firebase\JWT\JWT;

  // read raw POST data (JSON data) | not available with enctype="multipart/form-data"
  $data = json_decode(file_get_contents("php://input"));

  $request = $data->request;

  if($request == 'user_login') {

    // autoload classes
    include 'autoload.php';

    // get database connection
    $instance = Database::getInstance();
    $db = $instance->getConnection();

    // instantiate user_login object
    $user_login = new UserLogin($db);

    // set product property values
    $user_login->email = $data->email;
    $email_exists = $user_login->emailExists();
    
    // check if email exists and if password is correct
    if($email_exists && password_verify($data->password, $user_login->password)) {
    
      $token = array(
        "iss" => $iss,
        "aud" => $aud,
        "iat" => $iat,
        // "nbf" => $nbf, // not before | tipa, ne pre otvaranja radnih mesta u 9AM :)
        "exp" => $exp,
        "data" => array(
          "id" => $user_login->id,
          "company_id" => $user_login->commpany_id,
          "role_id" => $user_login->role_id,
          "first_name" => $user_login->first_name,
          "last_name" => $user_login->last_name,
          "email" => $user_login->email,
          "key" => $user_login->reg_key
        )
      );

      // set response code
      http_response_code(200);

      // generate jwt
      $jwt = JWT::encode($token, $key);
      // $jwt = "ce ga napravimo naknadno...";
      echo json_encode(
        array(
            "message" => "Successful login",
            "id" => $user_login->id,
            "company_id" => $user_login->commpany_id,
            "role_id" => $user_login->role_id,
            "first_name" => $user_login->first_name,
            "last_name" => $user_login->last_name,
            "jwt" => $jwt,
            "key" => $user_login->reg_key
        )
      );

    } else {

      // login failed

      // set response code
      http_response_code(401);

      // tell the user login failed
      echo json_encode(array("message" => "Login failed"));
    }
  }