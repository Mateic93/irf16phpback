<?php
// show error reporting
error_reporting(E_ALL);
 
// set your default time-zone
date_default_timezone_set('Europe/Belgrade');
 
// variables used for jwt | I randomly generated $key to for signature
$key = "BqEgJx6YIb9Als0AfOMSTU6a1682VudckstzgZifAU50XQBbk2zcNYDJhMOCcwNJ1OljZz0rB-BAfWSgIKb4v0W0Lk8uukJsTjFG7g3NPVi1hqTikuztPiEwOfRv7mb-Roaav24yWYF557OLNh7ng597RLjApaLviRtW50ZB1vemDOOEfU36a2ANiuQL3V66W_8GD-n15yNBzLXDdnzaj6jVHRTNtRGWGQciPQzfD4Zx2Y4QfcEtBJgz7orYYOCqnmsifrr4zxx2JpfUExabgBOBFPa8Yw6lD9dr0WjSCwEOh81JDgqlp4BG0K56pbTwhOwAwH5FokFVrUjyKHBl0Q";
$iss = "codehive"; // who issued the key? sure, it was codehive
$aud = "all ya great people"; // identifies the recipients that the JWT is intended for
$iat = time(); // (issued at) identifies the time at which the JWT was issued
// $nbf = time() + 10; // optional (not before) identifies the time before which the JWT MUST NOT be accepted for processing
$exp = time() + 3000; // optional (expiration time in seconds) which identifies the expiration time after which the JWT MUST NOT be accepted for processing.