<?php
/*
>>>BAZA<<<<
1) admin_requests => zahtevi
2) Users => Nakon registracije ovde idu 
3) Companies => Pravi se kad se registruje super_admin
4) 

Za sad funkcionise po sledecem principu:

-> Vue puca na API-je koji su nam sacuvani u php delu naseg domena
-> API-ji su tako napravljeni da u core fajla stoje sami API-ji koji primaju podatke tako sto zahtevaju prvo da se pozovu header() funkcije koje salju apachu sta sme i mora da radi
-> Podatke primaju kroz json_decode(file_get_contents("php://input")); koji je jedan JSON fajl sa svojim parametrima
-> svaki API zahteva da u $data koji se dobija ima i request polje koje vue salje i u zavisnosti od toga da li je request jednak onome sto se ocekuje izvrsavaju se funkcije
-> svaki API poziva klase iz Classes folder koristeci autoload.php 
-> Prva i glavna klasa jeste Database koja u konstructu vraca instancu klase
-> Posto pri prvom pristupu sajtu potencijalni korisnici imaju opciju da samo zahtevaju da koriste aplikaciju, prijavljuju se preko forme koja puca na create_user_request.php API koji poziva UserRequestData.php klasu. Ako je sve u redu, osobi se generise 6ocifreni kod za registraciju koristeci rekurzivnu funkciju klase RandKey, zahtev se unosi u bazu u tabelu admin_requests.
-> Radi se background check svih requestova i ako je odobreno salje se link korisniku koji se generise kroz skriptu koju je napravio Acko
-> Kada je korisnik dobio taj link on salje zahtev da se registruje kao super_admin svoje firme i puca na API user_create.php koji u pozadini vuce User.php klasu (ovo jos uvek sve moze da se menja). Buduci da vec postoje podatci o super_adminu kada je slao zahtev, mogu da se koriste ti podatci ili mozemo da zahtevamo od njega da ih da ponovo (ovo je sve do dogovara). Treba takodje razdvojiti da li se pri registraciji super_admina kreira i nova kompanija sto mislim da je potrebno ili lista komapnija vec postoji u sistemu na neki nacin. Sam user_create.php jos uvek ima da se radi posto nema sve provere (ostavio sam komentare tamo)
-> Nakon sto je korisnik kreiran koristi i pokusava da se loguje koristi se user_login.php koji proverava da li su podatci tacni, da li email postoji koristeci email_exists.php kao i odgovarajucu Email klasu
!!!!!!BITNO!!!!!!
-> pri logovanju koristimo JWT https://jwt.io/ koji omogucava bolju verziju kontrole i komunikacije izmedju fronta i backa od sesija
-> pri svakom logovanju se proverava da li je JWT korektan
--END SUPER BITNO--


>>>>>>DEO ZA ODRADITI<<<<<<<

Novi Matei Task:
-> Zavrsiti mass mailer koji je trenutno na codehive.rs
-> Odraditi api za approvovanje Admina
-> Odraditi
>>>>>>DEO ZA ODRADITI<<<<<<<

1. API admin_request

kada korisnik posalje request, API radi sledece:
	1. generise jedinstveni reg_key
	2. validira unose iz payload-a sa fronta
	3. smesti podatke u tabelu admin_requests (tabela ima default status pending)
	4. posalje mail ka nama bez ikakvih parametara samo sa linkom ka root-u ifrs app

PRVI API JE URADJEN!


2.API user_login

kada korisnik posalje request, API radi sledece:
	1. validira unose iz payload-a sa fronta
	2. pronadje postojeceg korisnika
	3. generise jwt i smesta u njega potrebne podatke o korisniku (id, company_id, f_name, l_name, email)
Napomena: za sada te iste podatke saljemo kroz response jer je tako lakse, ali bi pravi put bio dekodiranje jwt na frontu!


MATKETO RADI SLEDECE API-je:

--> Ovaj Deo Gotov <--
3. API admin_request_view

kada se ucita super_admin_dashboard, API radi sledece:
	1. selektuje sve iz admin_requests tabele
	2. generise i salje frontu objekat koji odvaja 'pending' od 'accepted'. 'denied' izostavlja
	{
		pending: data...,
		accepted: data...
	}
--> Ovaj Deo Gotov <--


--> Ovaj Deo Gotov <--
4. API approve_deny_admin


kada super_admin klikne na jedno od dva moguca polja ('approve', 'deny'), API radi sledece:
	1. prihvata payload sa fronta (payload sadrzi obavezan info o izboru i reg_key ili poruku razloga odbijanja)
	2. u zavisnosti od izbora, menja status u admin_requests tabeli sa 'pending' na 'accepted' ili 'deny
	3. u slucaju 'approve' salje mail korisniku sa linkom za finalizaciju registracije, a kao parametar salje reg_key
	4. u slucaju 'deny' salje mail korisniku sa porukom o razlogu odbijanja i setuje deny_reason u admin_requests tabeli
--> Ovaj Deo Gotov <--

5. API admi=n_request_approval

u slucaju 'approve' link za finalizaciju API radi sledece:
	1. prihvata reg_key koji je poslat kao get parametar
	2. pronalazi korisnika u admin_requests tabeli po reg_key (unique je, no worries :))
	3. smesta korisnika u users tabelu (ovaj api se bavi iskljucivo adminima pa u users tabeli dodeljuje role_id=2 (admin))
	4. smesta podatke kompanije u companies tabelu i pravi vezu u users tabeli na polju company_id


	-->Odraditi 5i deo <--
	5. generise novi reg_key i smesta ga u session_key polje tabele users po istom principu AdminRequestData klase


6. API employee_invite

kada admin zaposlenom posalje invite putem maila API radi sledece:
	1. prihvata payload sa fronta (payload je niz objekata i sadrzi email zaposlenog (zaposlenih), parametre privilegija, admin_id, company_id)
	2. zaposlenom salje email koji sadrzi link ka stranici za registraciju
	3. u mail dodaje stavke iz payloda, zakljucno sa mailom, ali ih prethodno enkriptuje koristeci base64 alg


7. API employee_registration

kada employee klikne na link u mailu odlazi na stranicu za registraciju gde unosi svoje licne podatke, front dekriptuje get parametre, a nakon slanja API radi sledece:
	1. prihvata payload sa fronta
	2. validira unose
	3. smesta korisnika u users tabelu (ovaj api se bavi iskljucivo employee-ima pa u users tabeli dodeljuje role_id=3 (employee)), acompany_id dobija iz parametara

*/

